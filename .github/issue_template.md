<!-- Hello, user! We suggest you to fill this
template to prevent duplicates and/or invalid issues!
-->

Variable | You value
---------|----------
Type | [Bug, Suggestion, etc.]
Browser | [Firefox (Quantum), Edge, Chrome...]
Page | https://vitalkanev.github.io/[...]

**Description**:

**Steps to reproduce**:

<!-- Replace everything in [] to your values
and put description + steps to reproduce above
~ VitalKanev -->
